const path = require('path')
const ShopifyHelper = require('../../helpers/shopify')
const express = require('express')
const orders = express.Router()
const bodyParser = require('body-parser')
const axios = require('axios')
orders.use(bodyParser.json({limit: '50000mb'})) //parse JSON request bodies
orders.use(bodyParser.urlencoded({limit: '50000mb', extended: false})) //parse queries

/*orders.get('/getOrders', async (req, res) => {
    let request = await ShopifyHelper(req)
    let sinceId = req.headers.since_id

    request.order
        .list({ fulfillment_status: 'unfulfilled', limit: 250, since: sinceId })
        .then((orders) => {
          if(orders.length == 0) {
            res.json({})
          } else {
            res.json(orders)
          }
        })
        .catch((err) => console.log(err))
})*/

orders.get('/getOrders', async(req, res) => {
  let key = req.headers.api_key
  let password = req.headers.token
  let shop = req.headers.shop_name

  let financial_status = req.headers.financial_status
  let fulfillment_status = req.headers.fulfillment_status
  let ids = req.headers.ids
  let since_id = req.headers.since_id
  let status = req.headers.status
  let tagged = req.headers.tagged

  let url = `https://${key}:${password}@${shop}.myshopify.com/admin/api/2022-07/orders.json?limit=250`

  if(financial_status) url = `${url}&financial_status=${financial_status}`
  if(ids) url = `${url}&ids=${ids}`
  if(since_id) url = `${url}&since_id=${since_id}`
  if(status) url = `${url}&status=${status}`
  if(fulfillment_status) url = `${url}&fulfillment_status=${fulfillment_status}`
  if(tagged) url = `${url}&tag=EMG`

  urlCall(url, key, password, shop, res)
})

let allOrders = []
async function urlCall(url, key, password, shop, res) {
  console.log(url)
  var key = key
  var password = password 
  var shop = shop

  axios({
    method: 'get',
    url: url,
    responseType: 'json'
  })
  .then(async(orders) => {
      if(orders.headers.link) {
        orders.data.orders.forEach((order) => {
          allOrders.push(order)
        })
  
        var page_info = await getNextPageInfo(orders.headers.link)
        var rel = orders.headers.link.includes('previous')
  
        if(rel) {
          res.json(allOrders)
          allOrders = []
        } else {
          urlCall(`https://${key}:${password}@${shop}.myshopify.com/admin/api/2022-07/orders.json?${page_info}&limit=250`, key, password, shop, res)
        }
      } else {
        if(allOrders.length === 0) {
          res.json(orders.data.orders)
          allOrders = []
        } else {
          orders.data.orders.forEach((order) => {
            allOrders.push(order)
          })
          res.json(allOrders)
          allOrders = []
        }
      }
  })
}

function getNextPageInfo(link) {
  let headers = link.toString()
  var page_info = link.substring(
    headers.indexOf("page_info"),
    headers.indexOf(">")
  )

  return page_info
}

module.exports = orders
