const path = require('path')
const ShopifyHelper = require('../../helpers/shopify')
const express = require('express')
const fulfillment = express.Router()
const bodyParser = require('body-parser')
const { resolve } = require('path')
fulfillment.use(bodyParser.json({limit: '50mb'})) //parse JSON request bodies
fulfillment.use(bodyParser.urlencoded({limit: '50mb', extended: false})) //parse queries

fulfillment.post('/fulfillOrders', async (req, res) => {
    let request = await ShopifyHelper(req)
    let fulfillmentOrder = await getFulfillment(request, req.body.order_id)
    let obj = await buildObject(fulfillmentOrder, req.body, req)

    request.fulfillment
        .createV2(obj)
        .then((data) => res.json(data))
        .catch((err) => res.json(err))
})


function getFulfillment(request, order) {
    return new Promise((resolve, reject) => {
        request.order
        .fulfillmentOrders(order)
        .then((data) => resolve(data))
        .catch((err) => reject(err))
    })

}

async function buildObject(order, body, req) {
    let line_items = await getFulfillmentOrderLineID(order[0].line_items, body.line_items, req)
    return new Promise((resolve, reject) => {
        let fulfillment = {
            "notify_customer": true,
            "tracking_info": {
                "number": body.tracking_numbers[0]
            },
            "line_items_by_fulfillment_order": [
                {
                    "fulfillment_order_id": order[0].id,
                    "fulfillment_order_line_items": line_items
                }
            ]
        }

        resolve(fulfillment)
    })
}

function getFulfillmentOrderLineID(fulfillment_items, items, req) {
    return new Promise((resolve, reject) => {
        let items_arr = []
        items.forEach(async (item, index) => {
            index ++
            let item_id = await getProducts(item.id, req)
            fulfillment_items.forEach((fulfill_item) => {
                if(fulfill_item.inventory_item_id == item_id) {
                    items_arr.push({"id": fulfill_item.id, "quantity": item.quantity})
                }
            })

            if(index == items.length) {
                resolve(items_arr)
            }
        })
    })
}

function getProducts(item, req) {
    return new Promise(async (resolve, reject) => {
        let request = await ShopifyHelper(req)

        request.product
            .list()
            .then((products) => {
                products.forEach((product) => {  
                    product.variants.forEach((variant) => {
                        if(item == variant.sku) {
                            resolve(variant.inventory_item_id)
                        }
                    })
                })
            })
    })
}
module.exports = fulfillment
