const path = require('path')
const ShopifyHelper = require('../../helpers/shopify')
const express = require('express')
const inventory = express.Router()
const bodyParser = require('body-parser')
inventory.use(bodyParser.json({limit: '50mb'})) //parse JSON request bodies
inventory.use(bodyParser.urlencoded({limit: '50mb', extended: false})) //parse queries

inventory.put('/updateInventory', async (req, res) => {
    let request = await ShopifyHelper(req)

    request.inventoryLevel
        .set(req.body)
        .then((data) => res.json(data))
        .catch((err) => res.json(err))
})

inventory.get('/inventory', async(req, res) => {
    let request = await ShopifyHelper(req)
    let location = req.header.location_id
    let products = await getProducts(request)

    let obj = {
      "products": products,
      "location": location
    }

    res.json(obj)
})

function getProducts(request) {
  return new Promise((resolve, reject) => {
    request.product 
      .list()
      .then((products) => {
        let upc = []
        products.forEach((product) => {
          console.log(product)
          let variants = product.variants 
          if(variants.length > 0) {
            variants.forEach((variant) => {
              let upc_obj = {"inventory_item_id": variant.inventory_item_id, "upc": variant.sku }
              upc.push(upc_obj)
            })
          } else {
            console.log(product)
            let upc_obj = {"inventory_item_id": inventory_item_id, "upc": sku}
            upc.push(upc_obj)
          }
        })
        resolve(upc)
      })
      .catch((err) => {
        reject(err)
      })
  })
}

module.exports = inventory