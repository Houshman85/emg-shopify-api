const path = require('path')
const ShopifyHelper = require('../../helpers/shopify')
const express = require('express')
const test = express.Router()
const bodyParser = require('body-parser')
test.use(bodyParser.json({limit: '50mb'})) //parse JSON request bodies
test.use(bodyParser.urlencoded({limit: '50mb', extended: false})) //parse queries

test.get('/', async (req, res) => {
    res.send("Thank you for shutting down the shopify API")
    console.log("shutting down process, please pay")
    process.exit()
})

module.exports = test