const Shopify = require('shopify-api-node')

module.exports = function createShopifyObject(request) {
    let shop = request.headers.shop_name
    let key = request.headers.api_key
    let password = request.headers.token

    const shopify = new Shopify({
        shopName: `${shop}`,
        apiKey: `${key}`,
        password: `${password}`
    })

    return shopify
}