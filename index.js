const path = require('path')
const express = require('express')
const app = express()
const helmet = require('helmet')

const PORT = process.env.PORT || 3000

const orders = require(path.join(__dirname, 'endpoints', 'orders', 'getOrders.js'))
const inventory = require(path.join(__dirname, 'endpoints', 'inventory', 'updateInventory.js'))
const fulfill = require(path.join(__dirname, 'endpoints', 'fulfillment', 'fulfillOrders.js'))
const test = require(path.join(__dirname, 'endpoints', 'test', 'test.js'))

app.use('/orders', orders)
app.use('/inventory', inventory)
app.use('/fulfill', fulfill)
app.use('/test', test)

app.use(helmet())

app.listen(PORT, () => {
    console.log('Server running on port ', PORT)
})